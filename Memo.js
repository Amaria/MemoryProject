// variables 
var t = 1;
var id1;
var isCountdownActive = false;

// Élements HTML
var countUpElement = document.getElementById('countUp');
var startElements = [].slice.call(document.querySelectorAll('.start'));
var stopElement = document.getElementById('stop');

console.log('startElements', startElements);
// fonctions au clic sur les boutons
startElements.map(function (div) { div.addEventListener('click', start) });
if (stopElement != null) {
  stopElement.addEventListener('click', stop);
}

function compteur() {
  countUpElement.innerHTML = t++ + "s";
}

function start() {
  if (!isCountdownActive) {
    isCountdownActive = true;
    id1 = setInterval(compteur, 1000)
  }
}

function stop() {
  clearInterval(id1);
  isCountdownActive = false;
}

function set() {
  var elt = $('input[name=name]').val();
  localStorage.setItem("username",elt);
}

$('.PlayerName').html("<h4> Salut " + localStorage.getItem("username") + " !</h4>");

var app = {
  cards:
  [ 'A', 'A', 'B', 'B',
    'C', 'C', 'D', 'D',
    'E', 'E', 'F', 'F',
    'G', 'G', 'H', 'H',
    'I', 'I', 'J', 'J'],

  init: function () {
    app.shuffle();
  },

  shuffle: function () {
    var random = 0;
    var temp = 0;
    for (i = 1; i < app.cards.length; i++) {
      random = Math.round(Math.random() * i);
      temp = app.cards[i];
      app.cards[i] = app.cards[random];
      app.cards[random] = temp;
    }
    app.assignCards();
    console.log('Shuffled Card Array: ' + app.cards);
  },

  assignCards: function () {
    $('.card').each(function (index) {
      $(this).attr('data-card-value', app.cards[index]);
    });
    app.clickHandlers();
  },

  clickHandlers: function () {
    var c = 0;
    $('.card').on('click', function () {

      $(this).html('<p>' + $(this).data('cardValue') + '</p>').addClass('selected');

      c++
      $('.level').html(c);

      app.checkMatch();
    });
  },

  checkMatch: function () {
    if ($('.selected').length === 2) {

      if ($('.selected').first().data('cardValue') == $('.selected').last().data('cardValue')) {
        $('.selected').each(function () {
          $(this).animate({
            opacity: 0
          }).removeClass('unmatched');
        });

        $('.selected').each(function () {
          $(this).removeClass('selected');
          $('.depot').append($('.selected').data('cardValue'));
        });


        app.checkWin();
      }
      else {
        setTimeout(function () {
          $('.selected').each(function () {
            $(this).html('').removeClass('selected');
          });
        }, 900);
      }
    }
  },

  checkWin: function () {

    if ($('.unmatched').length === 0) {
      $('.container').html('<h1>Gagné !</br></br>Bravo</h1>');

      clearInterval(id1);
      isCountdownActive = false;
    
    }


  }
};
app.init();



